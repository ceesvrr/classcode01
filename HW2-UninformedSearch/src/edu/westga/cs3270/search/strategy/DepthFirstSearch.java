package edu.westga.cs3270.search.strategy;

import edu.westga.cs3270.search.model.State;
import java.util.Stack;

/**
 * Represents the DFS algorithm - uses stack data structure
 * 
 * @author CS 3270
 * @version Fall 2018
 */
public class DepthFirstSearch {

	private State startState;
	private State goalState;

	/**
	 * Initializes the start and goal states of the search strategy.
	 * 
	 * @param start
	 * @param goal
	 */
	public DepthFirstSearch(State start, State goal) {
		this.startState = start;
		this.goalState = goal;
	}

	/**
	 * Executes the search in a state graph.
	 * 
	 * @return true if a solution exists, false otherwise
	 */
	public boolean search() {
		if (this.startState.equals(this.goalState)) {
			System.out.println("Solution found.");
		}

		Stack<State> stack = new Stack<State>();
		stack.add(this.startState);
		do {
			State currentState = stack.pop();
			if (currentState.equals(this.goalState)) {
				System.out.println("Solution Found: " + currentState.toString());
				return true;
			} else if (!currentState.getSuccessors().isEmpty()) {
				for (int index = currentState.getSuccessors().size() - 1; index >=0; index--) {
					stack.add(currentState.getSuccessors().get(index));
				}
			}
		} while(!stack.isEmpty());

		
		System.out.println("Solution Not Found" );
		return false;
	}

}