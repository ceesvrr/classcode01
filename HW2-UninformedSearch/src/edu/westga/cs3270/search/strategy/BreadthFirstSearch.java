package edu.westga.cs3270.search.strategy;

import edu.westga.cs3270.search.model.State;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Represents the BFS algorithm - uses queue data structure
 * 
 * @author CS 3270
 * @version Fall 2018
 */
public class BreadthFirstSearch {

	private State startState;
	private State goalState;

	/**
	 * Initializes the start and goal states of the search strategy.
	 * 
	 * @param start
	 * @param goal
	 */
	public BreadthFirstSearch(State start, State goal) {
		this.startState = start;
		this.goalState = goal;
	}

	/**
	 * Executes the search in a state graph.
	 * 
	 * @return true if a solution exists, false otherwise
	 */
	public boolean search() {
		if (this.startState.equals(this.goalState)) {
			System.out.println("Solution found.");
		}
		
		Queue<State> queue = new LinkedList<State>();
		
		//TO DO
		return false;

	}

}