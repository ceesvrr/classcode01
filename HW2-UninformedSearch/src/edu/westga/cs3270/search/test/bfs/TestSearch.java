package edu.westga.cs3270.search.test.bfs;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import edu.westga.cs3270.search.model.State;
import edu.westga.cs3270.search.strategy.BreadthFirstSearch;

class TestSearch {

	@Test
	void testValidSearch() {
		State state2 = new State("B", null, null);
		State state1 = new State("A", state2, null);
		BreadthFirstSearch bfs = new BreadthFirstSearch(state1, state2);
		assertEquals(true, bfs.search());
	}
	
	//TO DO

}
