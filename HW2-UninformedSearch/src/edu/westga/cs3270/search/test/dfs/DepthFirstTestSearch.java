package edu.westga.cs3270.search.test.dfs;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import edu.westga.cs3270.search.model.State;
import edu.westga.cs3270.search.strategy.BreadthFirstSearch;
import edu.westga.cs3270.search.strategy.DepthFirstSearch;

class DepthFirstTestSearch {

	@Test
	void testValidSearch() {
		State state2 = new State("B", null, null);
		State state1 = new State("A", state2, null);
		DepthFirstSearch dfs = new DepthFirstSearch(state1, state2);
		assertEquals(true, dfs.search());
	}
	
	//TO DO
	@Test
	void testMultipleSuccessorSearch() {
		State child = new State("Child1", null, null);
		State child2 = new State("Child 2", null, null);
		State goal = new State("Goal", child2, null);
		State start = new State("Start", child, goal);
		DepthFirstSearch dSearch = new DepthFirstSearch(start, child2);
		assertEquals(true, dSearch.search());
	}
}
