package edu.westga.cs3270.search.model;

import java.util.ArrayList;

/**
 * The State class represents a node in a state graph structured as a binary
 * tree, for example:
 * 
 *     Start
 *     /   \
 *    A     B
 *   / \   / \
 *  C   D E   F
 *     /       \
 *   Goal       H
 *
 * @author CS 3270
 * @version Fall 2018
 *
 */
public class State {

	private String stateName;
	private State leftChild;
	private State rightChild;

	/**
	 * 
	 * Creates a State in a binary tree with the given name and left/right
	 * successors.
	 * 
	 * @param stateName
	 * @param firstChild
	 * @param secondChild
	 */
	public State(String stateName, State firstChild, State secondChild) {
		this.stateName = stateName;
		this.leftChild = firstChild;
		this.rightChild = secondChild;
	}

	/**
	 * Returns the current state's successors.
	 * 
	 * @return successors The list of successor state(s)
	 */
	public ArrayList<State> getSuccessors() {

		ArrayList<State> successors = new ArrayList<State>();
		if (this.leftChild != null) {
			successors.add(this.leftChild);
		}
		if (this.rightChild != null) {
			successors.add(this.rightChild);
		}
		return successors;
	}

	@Override
	public String toString() {
		return this.stateName;
	}

}
